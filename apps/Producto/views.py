from django.shortcuts import render
from .models import Producto
from .forms import ProductoForm
from django.shortcuts import redirect, get_object_or_404
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.db.models import Q
from django.http import HttpResponse
# Create your views here.


# Producto

def filtro_Producto(request, pk):
    producto = Producto.objects.filter(cat_id=pk)
    return render(request, "Producto/filtrar_categoria.html", {'productos': producto})

def producto(request, pk):
    producto = get_object_or_404(Producto, pk=pk)
    prod = Producto.objects.filter(producto_id=pk)
    return render(request, 'Productos/pagina_catalogo.html', {'productos': producto, 'prod': prod})

class ProductoCreate(CreateView):
    model = Producto
    form_class = ProductoForm
    template_name = 'Productos/producto_form.html'
    success_url = reverse_lazy("listar_producto")

class ProductoList(ListView):
    model = Producto
    template_name = 'Productos/listar_producto.html'
    # paginate_by = 4

class ProductoUpdate(UpdateView):
    model = Producto
    form_class = ProductoForm
    template_name = 'Productos/producto_form.html'
    success_url = reverse_lazy('listar_producto')

class ProductoDelete(DeleteView):
    model = Producto
    template_name = 'Productos/producto_delete.html'
    success_url = reverse_lazy('listar_producto')

class SearchResultsView(ListView):
    model = Producto
    template_name = 'Productos/search.html'
    
def get_queryset(self): 
    query = self.request.GET.get('q')
    object_list = Producto.objects.filter(
    Q(nombre__icontains=query) | Q(descripcion__icontains=query))
    return object_list